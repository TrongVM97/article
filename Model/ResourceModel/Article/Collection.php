<?php


namespace Smartosc\Article\Model\ResourceModel\Article;




use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
protected function _construct()
{
    parent::_construct();
    $this->_init(\Smartosc\Article\Model\Article::class,\Smartosc\Article\Model\ResourceModel\Article::class);

}
//    public function getAllArticle(){
//        return $this->getItems();
//    }


}
