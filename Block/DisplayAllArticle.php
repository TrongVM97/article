<?php


namespace Smartosc\Article\Block;


use Magento\Framework\View\Element\Template;
use Smartosc\Article\Model\ResourceModel\Article\Collection;
use Smartosc\Article\Model\ResourceModel\Article\CollectionFactory;

class DisplayAllArticle extends Template
{
    protected $_collectionFactory;

    public function __construct(Template\Context $context,CollectionFactory $collectionFactory)
    {
        $this->_collectionFactory =$collectionFactory;
        parent::__construct($context);

    }
    public function loadAllArticles(){
        $collection = $this->_collectionFactory->create();
        $collection->addAttributeToSelect('*');
       // $a = $collection->getData();
        return $collection;
    }
}